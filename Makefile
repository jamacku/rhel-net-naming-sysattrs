.PHONY: usage install

UDEV_DIR := $(shell pkg-config --variable=udev_dir udev)
RULES_D  := $(UDEV_DIR)/rules.d
HWDB_D   := $(UDEV_DIR)/hwdb.d
DRACUT_MODULES_D := $(shell pkg-config --variable=dracutmodulesdir dracut)

usage:
	@echo -e 'Usage:\nmake DESTDIR=... install'

install:
	@if [ -z "$(UDEV_DIR)" ]; then echo "pkg-config failed to provide udev_dir."; exit 1; fi
	@if [ -z "$(DRACUT_MODULES_D)" ]; then echo "pkg-config failed to provide dracutmodulesdir."; exit 1; fi
	install -p -m644 -D -t $(DESTDIR)$(RULES_D) 74-rhel-net-naming-sysattrs.rules
	install -p -m644 -D -t $(DESTDIR)$(HWDB_D)  50-net-naming-sysattr-allowlist.hwdb
	install -p -m755 -D -t $(DESTDIR)$(DRACUT_MODULES_D)/74rhel-net-naming-sysattrs dracut/74rhel-net-naming-sysattrs/module-setup.sh
