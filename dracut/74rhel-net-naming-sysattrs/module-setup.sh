#!/bin/bash

# Make sure we always include generated link files in initrd
check() {
  return 0
}

depends() {
  echo hwdb
  return 0
}

install() {
  inst_rules "74-rhel-net-naming-sysattrs.rules"
}
